console.log("hello mundo");


// >> Synch Proof
// JavaScript is by default synchronous, meaning that it only executes on statement at a time.
/*
console.log("Hello Mundo p.2");

conosole.log("Hello again");
//code blocking -waiting for the specific statment to finish before executing to the next statement
console.log("Goodbye");
*/
/*
for (let i=0; i<=1500; i++){
    console.log(i);
}
console.log("hello again")
*/

// Asynchronous means that we can proceed to execute other statement, while time consuming code is running in the background

//the Fetch API that allows us to asynchronous request for a resource (data).
//"fetch()" method in JS is used to request to the server and load information on the webpages
// syntax :
// fetch("apiURL")

// [SECTION] getting all the posts

fetch("https://jsonplaceholder.typicode.com/posts")
.then(res => console.log(res.json()));

/*
    The ".then()" method captures the response object and returns a promise which will be "fullfilled" or "rejected"

    -.json () indentifies a network response and converts it directly to JS object

*/

fetch("https://jsonplaceholder.typicode.com/posts")
//.then(res => console.log(res));
.then(res => console.log(res.status));

fetch("https://jsonplaceholder.typicode.com/posts")
.then(res => res.json())// returns a response that is converted into JS object
.then(response => console.log(response));

// Translating arrow function to traditional function
/*
fetch("https://jsonplaceholder.typicode.com/posts")
.then(function(res){ 
	return res.json()
})
.then(function(response){ 
	return console.log(response)
})
*/

// [SECTION] Getting a specific post
// ":id" is a wildcard where you can put any value it then creates a link between "id" parameter in the URL and value provided in the URL
/*
fetch("https://jsonplaceholder.typicode.com/1")
.then(res => res.json())
.then(response => console.log(response));
*/

// The async and await keyword to achieve a synchronous code.

async  function fetchData(){
    let result = await(fetch("https://jsonplaceholder.typicode.com/posts"))

    console.log(result);

    let json = await result.json();
    console.log(json);

    console.log("hello world");
}

fetchData();

//GET POST PUT PATCH DELETE

//[GET] Getting a specific document
fetch("https://jsonplaceholder.typicode.com/posts/1")
.then(res => res.json())

//[POST] Inserting a document/field
/*
    Syntax:
        fetch("apiURL",{options})
         .then((response) => {})
         .then((response) => {})
*/
fetch("https://jsonplaceholder.typicode.com/posts",{
    
        method: "POST",
        headers:{
            "Content-Type" : "application/json"
        },
        body: JSON.stringify({
            title: "New Post",
            body: "Hello World",
            UserId: 1
        })
})

.then(response => response.json())
.then(json => console.log(json))

//[PUT] Updating a whole document (all fields)

fetch("https://jsonplaceholder.typicode.com/posts/1", {
        method: "PUT",
        headers:{
            "Content-Type" : "application/json"
        },
        body: JSON.stringify({
            title: "updated post",
            body: "hello again",
            userID: 1
        })
})

.then(response => response.json())
.then(json => console.log(json))

//[PATCH] Updating a specific field of a document
//PUT vs PATCH
    // PATCH is used to update single/several properties
    //PUT is used to update the whole document/object
fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "PATCH",
    headers: {
        "Content-Type" : "application/json"
    },
    body: JSON.stringify({
        title: "Corrected post title"
    })
})
.then(response => response.json())
.then(json => console.log(json))


//[DELETE] Deleting a document
fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "DELETE",   
})
.then(response => response.json())
.then(json => console.log(json))