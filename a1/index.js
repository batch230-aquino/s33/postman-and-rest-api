// console.log("Hello Mundo");


fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => console.log(res.json()));



fetch("https://jsonplaceholder.typicode.com/todos")

.then(res => res.json())


  .then((getData) => {

	let list = getData.map((todo => {
		return todo.title;
	}))

	console.log(list);

});



fetch("https://jsonplaceholder.typicode.com/todos/1")

.then(res => res.json())
.then(data => console.log(data));




fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then(res => res.json())
  .then(json => {
    const title = json.title;
    const completed = json.completed;
    console.log(`The item "${title}" on the list has a status of ${completed}`);
    
  });




  
  fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "Created to do list item",
        completed:false,
        userId: 1
    })
  })
    .then(res => res.json())
    .then(json => console.log(json));
  
  


fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "delectus aut autem",
        dateCompleted: "07/09/22",
        status: "complete",
        completed:false,
        userId: 1
    })
  })
    .then(response => response.json())
    .then(json => console.log(json));





fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "delectus aut autem",
        description: "To update the my to do list with a different data structure",
        status: "Pending",
        dateCompleted: "Pending",
        userId:1

        
    })
  })
    .then(response => response.json())
    .then(json => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1", {

      method: "PATCH",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
      dateCompleted: "07/09/21",
      status: "Complete"
  
          
      })
    })
      .then(response => response.json())
      .then(json => console.log(json));



fetch("https://jsonplaceholder.typicode.com/todos/1", {

      method: "DELETE",
})
.then(res => res.json())
.then(json => console.log(json))





